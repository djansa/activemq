# activemq-consumer-mongodb-boot-test

Project to test ActiveMQ and MongoDB with SpringBoot

[https://spring.io/guides/gs/spring-boot/](https://spring.io/guides/gs/spring-boot/)

## MongoDB Image

To install MongoDB Docker Image:

`docker pull mongo` 

To create and run the  __container__  with  __name__  and  __hostname__  ‘mongodb’. It needs to publish the  __port__  to access from host. Configure the  __volumes__  defined in the  __image__ :

`docker run --name mongodb --hostname mongodb -p 27017:27017 -v mongo_configdb:/data/configdb -v mongo_db:/data/db -d mongo:latest`	

To run the shell of ‘mongodb’:

`docker exec -it mongodb mongo`

To access to the  __contanier__  of ‘mongodb’:

`docker exec -it mongodb bash`

To stop the  __contanier__  ‘mongodb’:

`docker stop mongodb`

To run the  __container__  ‘mongodb’:

`docker start mongodb`

__MongoDB shell commands:__

```
show dbs
use test	
show collections
db.employee.count()
db.employee.remove({})
```

## SpringBoot ActiveMQ Consumer

This project need the  __activemq-producer-test project__ to send messages to ActiveMQ.

To start this springboot application:

```
mvn spring-boot:run
```

