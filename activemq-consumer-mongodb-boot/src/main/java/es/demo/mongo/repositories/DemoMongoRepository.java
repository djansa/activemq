package es.demo.mongo.repositories;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import es.demo.model.Employee;


@Repository
public interface DemoMongoRepository extends MongoRepository<Employee,String> {
    
	@Override
    List<Employee> findAll();           // find all Employee
   
	@Query(value = "{ 'empName' : ?0 }")
    List<Employee> findByEmpName(String empName);       // find employee by name
    
	@Query(value = "{ 'empId' : ?0 }")
	Employee findOne(String empId);           // find
    
}
