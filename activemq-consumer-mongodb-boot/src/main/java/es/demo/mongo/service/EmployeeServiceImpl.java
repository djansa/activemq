package es.demo.mongo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.demo.model.Employee;
import es.demo.mongo.repositories.DemoMongoRepository;

@Service
public class EmployeeServiceImpl implements EmployeeService {
	
    @Autowired
    private DemoMongoRepository employeeRepository;
    
    @Override
    public List<Employee> findAll() {
        return employeeRepository.findAll();
    }
    
    @Override
    public void delete(String employeeId) {
        employeeRepository.delete(employeeRepository.findOne(employeeId));
    }
    
    @Override
    public Employee save(Employee employee) {
        return employeeRepository.save(employee);
    }
    
    @Override
    public Employee update(Employee employee) {
        return employeeRepository.save(employee);
    }
}

