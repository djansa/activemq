package es.demo.mongo.service;

import java.util.List;

import es.demo.model.Employee;

public interface EmployeeService {
	
    List<Employee> findAll();
    
    Employee save(Employee employee);
    
    void delete(String employeeId);
    
    Employee update(Employee employee);
}