package es.demo.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import es.demo.exception.InvalidSalaryException;
import es.demo.model.Employee;
import es.demo.mongo.service.EmployeeService;

@Component
public class JMSConsumer {
	
	private static final Logger logger = LoggerFactory.getLogger(JMSConsumer.class);
	
	private final ObjectMapper mapper = new ObjectMapper();

	@Autowired
    private EmployeeService mongoService;
	
	@JmsListener(destination = "${activemq.queue}")
	public void consumeMessage(String msg) throws JsonMappingException, JsonProcessingException, InvalidSalaryException {
		
		Employee employee = mapper.readValue(msg, Employee.class);

		logger.info("Recieved Message From ActivetMQ: " + employee);
		
		if (employee.getSalary() < 0) {
			throw new InvalidSalaryException();
		}
		
		// SpringBoot MongoDB Test
		mongoService.save(employee);
	}
	
}
