# actrivemq-producer-test

Project to test actrivemq with SpringBoot

[https://spring.io/guides/gs/spring-boot/](https://spring.io/guides/gs/spring-boot/)

## ActiveMQ

To up the Docker ActiveMQ image `docker-compose up -d`. 

- Console `http://localhost:8161/admin/queues.jsp`

- User/pasword `admin/admin`

- Create queue `demo.queue`

To down the Docker ActiveMQ image `docker-compose down`.


## SpringBoot ActiveMQ Producer

To start springboot application:

```
mvn spring-boot:run
```

Link to test springboot application:

- [http://localhost:8080/activemq-test/](http://localhost:8080/activemq-test/)


Link to send a message to ActiveMQ queue:

- [http://localhost:8080/activemq-test/producer?empName=emp1&empId=emp001&salary=50](http://localhost:8080/activemq-test/producer?empName=emp1&empId=emp001&salary=50)


Test to send massive messages to ActiveMQ queue:

- `ProducerTest.java`



