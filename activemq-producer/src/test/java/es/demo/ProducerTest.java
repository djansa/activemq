package es.demo;

import java.net.HttpURLConnection;
import java.net.URL;

public class ProducerTest {


	private final static int numTest = 10;
	
	public static void main(String[] args) {
		
		// http://localhost:8080/activemq-test/producer?empName=emp1&empId=emp001&salary=50
		String strUrl = "http://localhost:8080/activemq-test/producer?";
		
    	URL url = null;
			
	    try {
			for (int i=0; i<numTest; i++) {
				
				StringBuffer s = new StringBuffer(strUrl)
						.append("empName=emp").append(i)
						.append("&empId=").append(i)
						.append("&salary=").append("1000");
				
				url = new URL(s.toString());
				
		        HttpURLConnection urlConn = (HttpURLConnection) url.openConnection();
		        urlConn.connect();
		        
		        System.out.println("URL - " + s.toString() + "Response Code : " + urlConn.getResponseCode());
			}

	    } catch (Exception e) {
	        System.err.println("Error creating HTTP connection");
	        e.printStackTrace();
	    }

	}

}
