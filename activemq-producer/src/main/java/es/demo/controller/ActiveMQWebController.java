package es.demo.controller;

import javax.jms.Queue;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;

import es.demo.model.Employee;

@RestController
@RequestMapping(value = "/activemq-test/")
public class ActiveMQWebController {

	@Autowired
	private JmsTemplate jmsTemplate;

	@Autowired
	private Queue queue;

	@GetMapping(value = "/producer")
	public String producer(@RequestParam("empName") String empName, @RequestParam("empId") String empId,
			@RequestParam("salary") int salary) {
		
		Employee emp = new Employee();
		emp.setEmpId(empId);
		emp.setEmpName(empName);
		emp.setSalary(salary);

		try {
			ObjectMapper mapper = new ObjectMapper();
			String empAsJson = mapper.writeValueAsString(emp);
	
			jmsTemplate.convertAndSend(queue, empAsJson);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return "Message sent to the ActiveMQ Successfully";
	}

	@RequestMapping("/")
	public String index() {
		return "Greetings from Spring Boot!";
	}

}
